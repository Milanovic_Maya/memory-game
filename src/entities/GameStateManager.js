class GameStateManager {
    constructor(numOfPairs) {
        this.numOfPairs = numOfPairs;
        this.numOfMatchingPairs = 0;
        this.maxScore = this.numOfPairs * 100;
        this.numOfMoves = 0;
        this.bestTimeBonus = this.numOfPairs * this.numOfPairs * 10;
        this.timeBonus = 0;
        this.movesBonus = 0;
        this.score = 0;
    };

    incNumberOfMovesPlayed(){
        this.numOfMoves++;
    }

    incNumberOfMatchesFound() {
        this.numOfMatchingPairs++;
    };

    calculateTimeBonus(time) {
        return this.timeBonus = this.bestTimeBonus - time * 10;
    };

    calculateMovesBonus() {
        const movesPercentage = 100 * (this.numOfPairs / this.numOfMoves);
        return this.movesBonus = parseInt(movesPercentage * this.maxScore / 100, 0);
    };

    getGameScore() {
        return this.score = this.movesBonus + this.timeBonus;
    };

    isGameOver() {
        return this.numOfPairs === this.numOfMatchingPairs;
    };

    onGameOver() {
        this.bestTimeBonus = this.numOfPairs * this.numOfPairs * 10;
        this.numOfMoves = 0;
        this.movesBonus = 0;
        this.numOfMatchingPairs = 0;
    };
};

export { GameStateManager };