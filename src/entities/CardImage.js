import { generateID } from "../shared/utilities";

class CardImage {
    constructor(url, groupId = generateID(), isOpened = false, id = "") {
        this.url = url;
        this.groupId = groupId;
        this.id = id;
        this.isOpened = isOpened;
    };
};

export { CardImage };