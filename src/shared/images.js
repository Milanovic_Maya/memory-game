export const images = {
    toons: [
        './images/toons/toons01.jpg',
        './images/toons/toons02.jpg',
        './images/toons/toons03.jpg',
        './images/toons/toons04.jpg',
        './images/toons/toons05.jpg',
        './images/toons/toons06.jpg',
        './images/toons/toons07.png',
        './images/toons/toons08.png',
        './images/toons/toons09.jpg',
        './images/toons/toons10.png',
        './images/toons/toons11.png',
        './images/toons/toons12.png'
    ],

    marvel: [
        './images/marvel/marvel01.png',
        './images/marvel/marvel02.png',
        './images/marvel/marvel03.png',
        './images/marvel/marvel04.png',
        './images/marvel/marvel05.png',
        './images/marvel/marvel06.png',
        './images/marvel/marvel07.jpg',
        './images/marvel/marvel08.jpg',
        './images/marvel/marvel09.jpg',
        './images/marvel/marvel10.jpg',
        './images/marvel/marvel11.jpg',
        './images/marvel/marvel12.jpg'
    ],

    dev: [
        './images/dev/dev01.png',
        './images/dev/dev02.png',
        './images/dev/dev03.png',
        './images/dev/dev04.jpg',
        './images/dev/dev05.png',
        './images/dev/dev06.png',
        './images/dev/dev07.png',
        './images/dev/dev08.png',
        './images/dev/dev09.png',
        './images/dev/dev10.jpg',
        './images/dev/dev11.png',
        './images/dev/dev12.png'
    ],
    
    anime: [
        './images/anime/anime01.jpg',
        './images/anime/anime02.jpg',
        './images/anime/anime03.jpg',
        './images/anime/anime04.jpg',
        './images/anime/anime05.png',
        './images/anime/anime06.png',
        './images/anime/anime07.jpeg',
        './images/anime/anime08.jpg',
        './images/anime/anime09.jpg',
        './images/anime/anime10.jpg',
        './images/anime/anime11.jpg',
        './images/anime/anime12.jpg'
]

}

export const cardDeck = {
    toons: './images/cardDeck/toons.jpg',
    marvel: './images/cardDeck/marvel.jpg',
    dev: './images/cardDeck/dev.jpg',
    anime: './images/cardDeck/anime.jpg'
}




