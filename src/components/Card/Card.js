import React from "react";
import "./Card.css";
import PropTypes from "prop-types";
import { cardDeck } from "../../shared/images";

const Card = ({ image, onCardOpen, selectedTheme }) => {

    if (!image.isOpened) {
        return (<div className={`card card-closed--${selectedTheme}`} onClick={() => onCardOpen(image)}>
            <img src={cardDeck[selectedTheme]} alt="deck" />
        </div>)
    }

    return (
        <div className="card card-opened animated flipInY">
            <img src={image.url} alt="imageLoremPicsum" />
        </div>
    );
};

Card.propTypes = {
    image: PropTypes.shape({
        url: PropTypes.string.isRequired,
        groupId: PropTypes.number.isRequired,
        isOpened: PropTypes.bool.isRequired
    }).isRequired
};

export { Card };