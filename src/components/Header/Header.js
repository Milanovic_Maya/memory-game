import React from "react";
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import "./Header.css";

const Header = ({ children, selectedLevel }) => {

    return (
    <header className="header">
        <div className={`header__container header__container--${selectedLevel}`}>
            <Link to='/'>
                <h1>MEMO</h1>
            </Link>
            {children}
        </div>
    </header>
)};

Header.propTypes = {
    children: PropTypes.node,
    selectedLevel: PropTypes.number
}

export { Header };