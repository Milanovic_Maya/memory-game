import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Timer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            gameRunningTime: 0,
            startTimer: false
        };

        this.timerInterval = null;

        this.printFormatedTime = this.printFormatedTime.bind(this);
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.runningTimer !== prevState.startTimer) {
            return {
                startTimer: nextProps.runningTimer,
            };
        }
        return null;
    };

    componentDidUpdate() {
        if (this.state.startTimer && !this.timerInterval) {
            clearInterval(this.timerInterval);
            this.timerInterval = null;

            this.timerInterval = setInterval(() => {
                this.startTimer();
            }, 1000);
        }
        
        if (this.props.isScoreReady) {
            clearInterval(this.timerInterval);
            this.props.gameOverTime(this.state.gameRunningTime);
        }

        if (!this.state.startTimer && this.timerInterval && !this.props.gameOver) {
            clearInterval(this.timerInterval);
            this.timerInterval = null;
            this.resetTimer();
        }
    };

    startTimer() {
        this.setState(prevState => ({ gameRunningTime: prevState.gameRunningTime + 1 }));
    };

    printFormatedTime(runningTime) {
        const t = Number(runningTime);
        let h = Math.floor(t / 3600);
        let m = Math.floor(t % 3600 / 60);
        let s = Math.floor(t % 3600 % 60);

        let hourSeparator = ':';
        let minuteSeparator = ':';

        if (h === 0) { h = ''; hourSeparator = ''; }
        if (m < 10 && h !== 0) { m = "0" + m; }
        if (s < 10) { s = "0" + s; }

        return `${h}${hourSeparator}${m}${minuteSeparator}${s}`;
    }

    resetTimer() {
        this.setState({ gameRunningTime: 0 });
    };

    render() {
        const { gameRunningTime } = this.state;

        return (
            <div>{this.printFormatedTime(gameRunningTime)}</div>
        )
    };
};

Timer.propTypes = {
    runningTimer: PropTypes.bool
};
Timer.defaultProps = {
    runningTimer: false
};

export { Timer };