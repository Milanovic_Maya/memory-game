import React from 'react';
import PropTypes from 'prop-types';

import './Levels.css'

const Levels = ({ onChooseLevel, currentLevel }) => {
    const activeClass = 'current-level'

    return (
    <div className="levels">
        <div id="6" onClick={onChooseLevel} className={currentLevel === 6 ? activeClass : ''}>Easy</div>
        <div id="8" onClick={onChooseLevel} className={currentLevel === 8 ? activeClass : ''}>Medium</div>
        <div id="10" onClick={onChooseLevel} className={currentLevel === 10 ? activeClass : ''}>Hard</div>
        <div id="12" onClick={onChooseLevel} className={currentLevel === 12 ? activeClass : ''}>Super Memory!</div>
    </div>
)}

Levels.propTypes = {
    onChooseLevel: PropTypes.func.isRequired,
    currentLevel: PropTypes.number.isRequired
}

export { Levels }