import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'
import { Main } from '../../pages/Main/Main';
import { HomePage } from '../../pages/HomePage/HomePage';

import './App.css'

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedTheme: "anime",
            numOfPairs: 8
        };

        this.onChooseTheme = this.onChooseTheme.bind(this);
        this.onChooseLevel = this.onChooseLevel.bind(this);
    }

    onChooseLevel(e) {
        const numOfPairs = parseInt(e.target.id, 0);
        this.setState({
            numOfPairs
        });
    };

    onChooseTheme(e) {
        const selectedTheme = e.target.id;
        this.setState({
            selectedTheme
        });
    };

    render() {
        const { selectedTheme, numOfPairs } = this.state;

        return (
            <Switch>
                <Route path='/game' render={() => <Main selectedTheme={selectedTheme} numOfPairs={numOfPairs} />} />
                <Route path='/' render={() =>
                    <HomePage
                        onChooseTheme={this.onChooseTheme}
                        onChooseLevel={this.onChooseLevel}
                        currentLevel={numOfPairs}
                        currentTheme={selectedTheme} />}
                />
            </Switch>
        )
    }
}

export { App }