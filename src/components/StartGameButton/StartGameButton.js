import React from 'react';
import { Link } from 'react-router-dom'

import './StartGameButton.css'

const StartGameButton = () => (
    <Link to='/game'><button className="start-game-button">NEW GAME</button></Link>
);

export { StartGameButton };